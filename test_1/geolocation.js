
const Geo = require('./pageobject/geolocation');

describe('geolocation tests', () => {
    it('should open, click button and check result', async () => {
        await Geo.open();
        await Geo.btn_clk();
        const latitude = await Geo.latitude.getText();
        const longitude  = await Geo.longitude.getText();
        var from_1 = 0;
        var to_1 = 8;
        var from_2 = 0;
        var to_2 = 8;
        var newstr_1 = latitude.substring(from_1,to_1);
        var newstr_2 = longitude.substring(from_2,to_2);

        await Geo.map_link_clk();
        await expect(browser).not.toHaveUrl('https://the-internet.herokuapp.com/geolocation')
        const google = await Geo.google_parameters.getText();
        const google_1 = await google.substring(0,8)
        const google_2 = await google.substring(11,19)
        await expect(newstr_1).toBe(google_1);
        await expect(newstr_2).toBe(google_2);
    });
});

