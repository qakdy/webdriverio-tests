const DynamicControl = require('./pageobject/dynamic_controls');

describe('Dynamic Control test', () => {
    it('should open and click checbox and check it,should click other buttons and check results', async () => {
        await DynamicControl.open();
        await DynamicControl.checkbox_clk();
        await expect(DynamicControl.checkbox).toBeChecked();
        await DynamicControl.rm_btn_clk();
        await expect(DynamicControl.message_rm).toHaveTextContaining("It's gone!");
        await DynamicControl.rm_btn_clk();
        await expect(DynamicControl.message_rm).toHaveTextContaining("It's back!");
        await DynamicControl.enable_clk();
        await expect(DynamicControl.message_en).toHaveTextContaining("It's enabled!");
        await DynamicControl.enable_clk();
        await expect(DynamicControl.message_en).toHaveTextContaining("It's disabled!");
    });
});
