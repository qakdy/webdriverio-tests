const KeyPress = require('./pageobject/keypress')

describe('keypress test', () => {
    it('should open page and check header,', async () => {
        await KeyPress.open();
        await expect(KeyPress.header).toHaveTextContaining('Key Presses');
    });
    it('should open page and check actions,', async () => {
        await browser.keys("ArrowRight");
        await expect(KeyPress.p_result).toHaveTextContaining('You entered: RIGHT');
        await browser.keys("ArrowLeft");
        await expect(KeyPress.p_result).toHaveTextContaining('You entered: LEFT');
        await browser.keys("ArrowDown");
        await expect(KeyPress.p_result).toHaveTextContaining('You entered: DOWN');
        await browser.keys("ArrowUp");
        await expect(KeyPress.p_result).toHaveTextContaining('You entered: UP');
        await browser.keys("Backspace");
        await expect(KeyPress.p_result).toHaveTextContaining('You entered: BACK_SPACE');
        await browser.keys("Tab");
        await expect(KeyPress.p_result).toHaveTextContaining('You entered: TAB');
        await browser.keys("Shift");
        await expect(KeyPress.p_result).toHaveTextContaining('You entered: SHIFT');
        await browser.keys("Escape");
        await expect(KeyPress.p_result).toHaveTextContaining('You entered: ESCAPE');
        await browser.keys("KeyA");
        await expect(KeyPress.p_result).toHaveTextContaining('You entered: A');
        await browser.keys("KeyB");
        await expect(KeyPress.p_result).toHaveTextContaining('You entered: B');
    });
});
