const FloatingMenu = require('./pageobject/floating_menu');

describe('floating menu test', () => {
    it('should open page and check buttons', async () => {
        await FloatingMenu.open();
        await FloatingMenu.home_clk();
        await expect(browser).toHaveUrlContaining('https://the-internet.herokuapp.com/floating_menu#home');
        await FloatingMenu.news_clk();
        await expect(browser).toHaveUrlContaining('https://the-internet.herokuapp.com/floating_menu#news');
        await FloatingMenu.contact_clk();
        await expect(browser).toHaveUrlContaining('https://the-internet.herokuapp.com/floating_menu#contact');
        await FloatingMenu.about_clk();
        await expect(browser).toHaveUrlContaining('https://the-internet.herokuapp.com/floating_menu#about');
    });
    it('scroll to the second paragraph ', async () => {
        await FloatingMenu.p2.scrollIntoView(); 
        await browser.pause(5000);
    });
    it('scroll to the last paragraph and check buttons', async () => {
        await FloatingMenu.p10.scrollIntoView();
        await browser.pause(5000);
        await FloatingMenu.home_clk();
        await expect(browser).toHaveUrlContaining('https://the-internet.herokuapp.com/floating_menu#home');
        await FloatingMenu.news_clk();
        await expect(browser).toHaveUrlContaining('https://the-internet.herokuapp.com/floating_menu#news');
        await FloatingMenu.contact_clk();
        await expect(browser).toHaveUrlContaining('https://the-internet.herokuapp.com/floating_menu#contact');
        await FloatingMenu.about_clk();
        await expect(browser).toHaveUrlContaining('https://the-internet.herokuapp.com/floating_menu#about');
    });

});