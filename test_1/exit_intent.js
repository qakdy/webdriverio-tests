
describe('exit intent', () => {
    it('should open page and check text header', async () => {
        await browser.url('https://the-internet.herokuapp.com/exit_intent')
        await expect($('h3')).toHaveTextContaining('Exit Intent');
    });
    // don.t work
    it('move up', async () => {
        await browser.maximizeWindow()
        await  $('body > div:nth-child(2) > a > img').moveTo({x:10,y:10})
    });
    it('check alert', async () => {
        await expect($('#ouibounce-modal > div.modal > div.modal-title')).toHaveTextContaining('THIS IS A MODAL WINDOW');
    });
    it('close window', async () => {
        await $('#ouibounce-modal > div.modal > div.modal-footer').click();
        await expect($('h3')).toHaveTextContaining('Exit Intent');
    });
});



