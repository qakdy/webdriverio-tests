const Drag_and_drop = require('./pageobject/drag_n_drop')

const dragAndDrop = require('html-dnd').codeForSelectors;

describe('drad and drop', () => {
    it('should open page and check text header', async () => {
        await Drag_and_drop.open();
        await browser.execute(dragAndDrop, Drag_and_drop.columnA.selector, Drag_and_drop.columnB.selector);
        await expect(Drag_and_drop.columnAHeader).toHaveText('B');
    });
});

