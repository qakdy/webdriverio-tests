describe('forgot password', () => {
    it('forgot password test', async () => {
        await browser.url('https://the-internet.herokuapp.com/')
        const el = await $('=Forgot Password')
        await expect(el).toBeClickable()
        await el.click()
        const title = await $('h2')
        await title.waitForDisplayed()
        await expect(title).toHaveTextContaining('Forgot Password')

        const email = await $('#email')
        await email.addValue('dima@mail.ru')
        const retrieve_pass = $('#form_submit')
        await expect(retrieve_pass).toBeClickable()
        await retrieve_pass.click()

        const title_h1 = await $('h1')
        await title_h1.waitForDisplayed()
        await expect(title_h1).toHaveTextContaining('Internal Server Error')
    })
})