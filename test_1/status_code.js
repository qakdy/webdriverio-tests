// import fetch from 'node-fetch'; -> skipped tests

const Status = require('./pageobject/status_code')
describe('status code tests', () => {
    it('should open page and click the link', async () => {
        await Status.open();
        await Status.link_redirect_clk();
        await expect(browser).toHaveUrlContaining('https://the-internet.herokuapp.com/status_codes')
    });
    it('should open page and with status code 200;check url and check status code', async () => {
        await Status.link_200_clk()
        await expect(browser).toHaveUrlContaining('https://the-internet.herokuapp.com/status_codes/200')
        // const url = 'https://the-internet.herokuapp.com/status_codes/200'
        // const response =  await fetch(url);
        // if (response.ok){
        //     await expect(browser).toHaveUrlContaining('https://the-internet.herokuapp.com/status_codes/200')
        // }; -> ERROR
    });
    it('should open page and with status code 301;check url', async () => {
        await Status.link_s_c_clk()
        await Status.link_301_clk()
        await expect(browser).toHaveUrlContaining('https://the-internet.herokuapp.com/status_codes/301')
    });
    it('should open page and with status code 404;check url', async () => {
        await Status.link_s_c_clk()
        await Status.link_404_clk()
        await expect(browser).toHaveUrlContaining('https://the-internet.herokuapp.com/status_codes/404')
    });
    it('should open page and with status code 500;check url', async () => {
        await Status.link_s_c_clk()
        await Status.link_500_clk()
        await expect(browser).toHaveUrlContaining('https://the-internet.herokuapp.com/status_codes/500')
    });
});