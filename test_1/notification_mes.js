const NotificationMessage = require('./pageobject/notification_mes');

describe('notification message test', () => {
    it('should open page and notification message shouldnt be existing, click link and check changes in notification message,', async () => {
        await NotificationMessage.open();
        await expect(NotificationMessage.not_mes).not.toBeExisting();
        await NotificationMessage.link_click();
        var text = await NotificationMessage.not_mes.getText();
        var from_1 = 0;
        var to_1 = 17;
        var from_2 = 0;
        var to_2 = 36;
        var newstr_1 = text.substring(from_1,to_1);
        var newstr_2 = text.substring(from_2,to_2);
        var opt_1 = 'Action successful';
        var opt_2 = 'Action unsuccesful, please try again';
        if (newstr_1 !== opt_1){
            await expect(newstr_2).toBe(opt_2)
        }
    });

    it('refresh page; notification message shouldnt be existing', async () => {
        await browser.refresh();
        await expect(NotificationMessage.not_mes).not.toBeExisting();
    });

    it('clk link and that notification_message is existing ;close message and that not_mes is not existing', async () => {
        await NotificationMessage.link_click();
        await expect(NotificationMessage.not_mes).toBeExisting();
    });

    it('maximize window, close notif. message; check changes and new url', async () => {
        await browser.maximizeWindow();
        await $("a.close").click();
        await expect(NotificationMessage.not_mes).not.toBeDisabled();
    });
}); 