
const DynamicLoad = require('./pageobject/dynamic_loading');

describe('dynamic loading', () => {
    it('should open and check text header', async () => {
        await DynamicLoad.open();
        await expect(DynamicLoad.header).toHaveTextContaining('Dynamically Loaded Page Elements');
    });

    it('click ex_1 link ; check header and find hidden element', async () => {
        await DynamicLoad.ex1_clk();
        await expect(browser).toHaveUrlContaining('https://the-internet.herokuapp.com/dynamic_loading/1');
        await expect(DynamicLoad.header_ex1).toHaveTextContaining('Example 1: Element on page that is hidden');
        await expect($('//*[@id="finish"]')).toBeExisting();
        await DynamicLoad.button_start_clk();
        await expect($('//*[@id="finish"]')).toHaveTextContaining('Hello World!');
    });

    it('refresh page ; page should have button "start", text "hello world" should disappear  ', async () => {
        await browser.refresh();
        await expect(DynamicLoad.button_start).toHaveTextContaining('Start');
    });

    it('refresh page ;  text(xpath selector) "hello world" should be disappear  ', async () => {
        await browser.refresh();
        await expect($('//*[@id="finish"]')).not.toHaveTextContaining('Hello World!');
    });

    it('click ex_2 link ; check header and url, hidden element in not existing ', async () => {
        await DynamicLoad.open();
        await DynamicLoad.ex2_clk();
        await expect(browser).toHaveUrlContaining('https://the-internet.herokuapp.com/dynamic_loading/2');
        await expect(DynamicLoad.header_ex1).toHaveTextContaining('Example 2: Element rendered after the fact');
        await expect($('//*[@id="finish"]')).not.toBeExisting();
    });
    
    it('click ex_2 link ; check header and find element rendered after the fact', async () => {
        await DynamicLoad.open();
        await DynamicLoad.ex2_clk();
        await expect(browser).toHaveUrlContaining('https://the-internet.herokuapp.com/dynamic_loading/2');
        await expect(DynamicLoad.header_ex1).toHaveTextContaining('Example 2: Element rendered after the fact');
        await DynamicLoad.button_start_clk();
        await expect(DynamicLoad.finish).toHaveTextContaining('Hello World!');
        await expect($('//*[@id="finish"]')).toBeExisting();
    });
});

