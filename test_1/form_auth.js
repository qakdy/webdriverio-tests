describe('login page', () => {
    it('login page test', async () => {
        await browser.url('https://the-internet.herokuapp.com/')
        const el = await $('=Form Authentication')
        await expect(el).toBeClickable()
        await el.click()
        const title = await $('h2')
        await title.waitForDisplayed()
        await expect(title).toHaveTextContaining('Login Page')

        const username = await $('#username')
        await username.addValue('tomsmith')
        const password = await $('#password')
        await password.addValue('SuperSecretPassword!')
        const Login = $('button')
        await expect(Login).toBeClickable()
        await Login.click()

        const title_h4 = await $('h4')
        await title_h4.waitForDisplayed()
        await expect(title_h4).toHaveTextContaining('Welcome to the Secure Area. When you are done click logout below.')

        const Logout = await $('a.button')
        await expect(Logout).toBeClickable()
        await Logout.click()

        await title.waitForDisplayed()
        await expect(title).toHaveTextContaining('Login Page')

    })
})