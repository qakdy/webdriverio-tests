const EntryAd = require('./pageobject/entry_ad')

describe('entry ad test', () => {
    it('should open page and check text header', async () => {
        await EntryAd.open();
        await expect(EntryAd.header).toHaveTextContaining('Entry Ad');
    });
    it('clk link "clikc here"', async () => {
        await EntryAd.clk_hr();
        await expect(EntryAd.header_window).toHaveTextContaining('THIS IS A MODAL WINDOW');
    });
    it('close window', async () => {
        await EntryAd.close_clk();
        await expect(EntryAd.header).toHaveTextContaining('Entry Ad');
    });
});
