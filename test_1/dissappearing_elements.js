const Dis = require('./pageobject/disappearing')
const Main = require('./pageobject/main')
const About = require('./pageobject/about')
const Contact = require('./pageobject/contact us')
const Portfolio = require('./pageobject/portfolio')
const Gallery =  require('./pageobject/gallery')



describe('dis el', () => {
    it('Should open and check header text ', async () => {
        await Dis.open();
        await expect(Dis.header).toHaveTextContaining('Disappearing Elements');
    });
    it('Schould clk the btn and check header in the main page', async () => {
        await Dis.open();
        await Dis.home_click();
        await expect(Main.header).toHaveTextContaining('Welcome to the-internet');
    });
    it('Schould clk the btn and check header in the about page', async () => {
        await Dis.open();
        await Dis.about_click();
        await expect(About.header).toHaveTextContaining('Not Found');
    });
    it('Schould clk the btn and check header in the  page contact us', async () => {
        await Dis.open();
        await Dis.contact_click();
        await expect(Contact.header).toHaveTextContaining('Not Found');
    });
    it('Schould clk the btn and check header in the portfolio ', async () => {
        await Dis.open();
        await Dis.portfolio_click();
        await expect(Portfolio.header).toHaveTextContaining('Not Found'); 
    });
    it(' Schould check btn in the page ', async () => {
        await Dis.open();
        const isExisting = await Dis.gallery_btn.isExisting();
        do {await browser.refresh()} while (isExisting === true);
        await Dis.gallery_click();
        await expect(Gallery.header).toHaveTextContaining('Not Found')
        
    });
});