
describe('add remove elements', () => {
  it('should click button and check amount of buttons', async () => {
    await browser.url('https://the-internet.herokuapp.com/add_remove_elements/')
    await $('button').click();
    await $$('button')[1].waitForDisplayed();
    var buttons = await $$("button");
    await expect(buttons.length).toBe(2);
  });
  it('remove button and check amount of buttons', async () => {
    await $('.added-manually').click()
    var buttons = await $$("button");
    await expect(buttons.length).toBe(1);
  });
  it('should click button and check amount of buttons', async () => {
    for (var i = 0 ; i < 3; i++){
      await $('button').click();
      var x = i + 1;
      await $$('#elements button')[i].waitForDisplayed();
      var buttons = await $$("#elements button");
      await expect(buttons.length).toBe(x);
    };
  });
});


