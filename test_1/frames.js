const Frames = require('./pageobject/frames');


describe('Frames test', () => {
    it('open page, click the first link "nested frames" and check new url', async () => {
        await Frames.open();
        await expect(Frames.header).toHaveTextContaining('Frames');
        await Frames.nested_frames_clk();
        await expect(browser).toHaveUrlContaining('https://the-internet.herokuapp.com/nested_frames');
    });
    it('switch to frame_left and check ', async () => {
        const frame_top = await $('frameset > frame:nth-child(1)[name = frame-top]');
        await browser.switchToFrame(frame_top);
        const frame_left = await $('frameset[name = frameset-middle] > frame:nth-child(1)[name = frame-left]')
        await browser.switchToFrame(frame_left);
        await expect($("body")).toHaveTextContaining('LEFT');
    });
    it('switch to frame_middle and check ', async () => {
        await browser.switchToParentFrame();
        const frame_middle = await $('frameset[name = frameset-middle] > frame:nth-child(2)[name = frame-middle]')
        await browser.switchToFrame(frame_middle);
        await expect($("#content")).toHaveTextContaining('MIDDLE');
    });
    it('switch to frame_right and check ', async () => {
        await browser.switchToParentFrame();
        const frame_right = await $('frameset[name = frameset-middle] > frame:nth-child(3)[name = frame-right]')
        await browser.switchToFrame(frame_right);
        await expect($("body")).toHaveTextContaining('RIGHT');
    });
    it('switch to frame_bottom and check ', async () => {
        await browser.switchToParentFrame();
        await browser.switchToParentFrame();
        const frame = await $('frameset>frame:nth-child(2)[name=frame-bottom]')
        await browser.switchToFrame(frame);
        await expect($("body")).toHaveTextContaining('BOTTOM');
    });
    it('open page, click the second link "iframe" and check new url', async () => {
        await Frames.open();
        await expect(Frames.header).toHaveTextContaining('Frames');
        await Frames.iframe_clk();
        await expect(browser).toHaveUrlContaining('https://the-internet.herokuapp.com/iframe');
    });
    it('switch to iframe and check text', async () => {
        const frame_text = await $('iframe');
        await browser.switchToFrame(frame_text);
        await expect(Frames.text_iframe).toHaveTextContaining('Your content goes here');
    });
    it('switch to iframe and check text, he should dissappear ', async () => {
        await browser.switchToParentFrame();
        await Frames.file_btn_clk();
        await Frames.new_doc_btn_clk();
        const frame_text = await $('iframe');
        await browser.switchToFrame(frame_text);
        await expect(Frames.text_iframe).toHaveTextContaining("");
    });
    it('click arrow back button, text should appear ', async () => {
        await browser.switchToParentFrame();
        await Frames.arrow_back_btn_clk();
        const frame_text = await $('iframe');
        await browser.switchToFrame(frame_text);
        await expect(Frames.text_iframe).toHaveTextContaining("Your content goes here");
        
    });
    it('click arrow forward button, text should diassappear ', async () => {
        await browser.switchToParentFrame();
        await Frames.arrow_forward_btn_clk();
        const frame_text = await $('iframe');
        await browser.switchToFrame(frame_text);
        await expect(Frames.text_iframe).toHaveTextContaining("");
    });
    it('switch to iframe and check text, he should dissappear ', async () => {
        await Frames.text_iframe.setValue('Hey, My name is Dima');
        await expect(Frames.text_iframe).toHaveTextContaining("Hey, My name is Dima");
    });
    it('browser refresh, check arrow buttons and text ', async () => {
        await browser.refresh();
        const frame_text = await $('iframe');
        await browser.switchToFrame(frame_text);
        await expect(Frames.text_iframe).toHaveTextContaining("Your content goes here");
    });

    
});


