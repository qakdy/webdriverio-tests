const Challenging_dom = require('./pageobject/challenging_dom');

describe('challenging dom', () => {
    it('should open and check text header', async () => {
        await Challenging_dom.open();
        await expect(Challenging_dom.header).toHaveTextContaining('Challenging DOM');
    });
    it('click all buttons and check to be clicable', async () => {
        await expect(Challenging_dom.btn).toBeClickable();
        await expect(Challenging_dom.btn_alert).toBeClickable();
        await expect(Challenging_dom.btn_scs).toBeClickable();
    });
    it('get attribute for all buttons', async () => {
        const atr_1 = await Challenging_dom.btn.getAttribute('class');
        await expect(atr_1).toBe('button');
        const atr_2 = await Challenging_dom.btn_alert.getAttribute('class');
        await expect(atr_2).toBe('button alert');
        const atr_3 = await Challenging_dom.btn_scs.getAttribute('class');
        await expect(atr_3).toBe('button success');
    });
    it('click the button and check text changes on the  button', async () => {
        const text_btn_1 = await Challenging_dom.btn.getText();
        const text_btn_alert_1 =  await Challenging_dom.btn_alert.getText();
        const text_btn_success_1 = await Challenging_dom.btn_scs.getText();
        await Challenging_dom.btn_clk();
        const text_btn_2 = await Challenging_dom.btn.getText();
        const text_btn_alert_2 =  await Challenging_dom.btn_alert.getText();
        const text_btn_success_2 = await Challenging_dom.btn_scs.getText();
        if (text_btn_1 === text_btn_2){
            if(text_btn_alert_1 === text_btn_alert_2){
                expect(text_btn_success_1).not.toBe(text_btn_success_2)
            } else {
                expect(text_btn_alert_1).not.toBe(text_btn_alert_2)
            }
        } else {
            expect(text_btn_1).not.toBe(text_btn_2)
        }   
    });
    it('click the button and check url #edit', async () => {
        await Challenging_dom.edit_clk();
        await expect(browser).toHaveUrlContaining('https://the-internet.herokuapp.com/challenging_dom#edit');
    });
    it('click the button and check url #delete', async () => {
        await Challenging_dom.delete_clk();
        await expect(browser).toHaveUrlContaining('https://the-internet.herokuapp.com/challenging_dom#delete');
    });
    it('check text in table', async () => {
        for(var i= 0; i< 10; i++){
            await expect($('tbody').$$('tr')[i]).toHaveTextContaining('Iuvaret'+[i]+' Apeirian'+[i]+' Adipisci'+[i]+' Definiebas'+[i]+' Consequuntur'+[i]+' Phaedrum'+[i]+' edit delete')
        };
    });
    xit('canvas element', async () => {
        await browser.url('https://the-internet.herokuapp.com/challenging_dom')
        let str =  await $('#canvas').getAttribute('innerHTML'); // -> null
        console.log('```````````````````', await str)
        let regexp = await 'Answer:\s(\d+)'
        let canvas = await  str.search(regexp)
        await expect(canvas).toBe('Answer:66555')
    });
});
