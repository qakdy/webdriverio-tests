const ShadowRoot = require('./pageobject/shadow_root')

describe('shadow root test', () => {
    it('should open page, check shadow text and compare with text outside the shadowroot ', async () => {
        await ShadowRoot.open();
        const text = await $('#content > my-paragraph:nth-child(4)').shadow$('p > slot');
        const shadow_text_1 = await text.getText();
        await expect(shadow_text_1).toBe('My default text')

        const text_1 = await $('#content > my-paragraph:nth-child(4) > span').getText();
        await expect(shadow_text_1).not.toBe(text_1)
    });
    
    it('should open page, check shadow text and compare with text 1 and 2 outside the shadowroot ', async () => {
        const text = await $('#content > my-paragraph:nth-child(5)').shadow$('p > slot');
        const shadow_text_1 = await text.getText();
        await expect(shadow_text_1).toBe('My default text')

        const text_1 = await $('#content > my-paragraph:nth-child(5) > ul > li:nth-child(1)').getText();
        await expect(shadow_text_1).not.toBe(text_1)
        const text_2 = await $('#content > my-paragraph:nth-child(5) > ul > li:nth-child(2)').getText();
        await expect(shadow_text_1).not.toBe(text_2)
    });
});