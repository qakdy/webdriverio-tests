const Secure = require('./pageobject/secure_file_download');

describe('secure tests', () => {
    it('open page, passing authorization and check header  ', async () => {
        await Secure.open();
        await expect(Secure.header).toHaveTextContaining('Secure File Downloader')
    });
});