const FileUploader = require('./pageobject/file_uploader');
const path = require('path');


describe('upload file test', () => {
    it('should open page, upload file, click button and check result', async () => {
        const filePath = await  path.join(__dirname,'./photo/bug.png');
        const remoteFilePath = await  browser.uploadFile(filePath);

        await FileUploader.open();
        await FileUploader.input.setValue(remoteFilePath);
        await FileUploader.upload_clk();
        await expect(FileUploader.result).toHaveTextContaining("bug.png");
    });
    it('should open page, NOT upload file, click button and check result', async () => {
        await FileUploader.open();
        await FileUploader.upload_clk();
        await expect(FileUploader.new_header).toHaveTextContaining("Internal Server Error");
    });
});
