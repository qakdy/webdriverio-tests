const MainPage = require ('./main_page')

class DownloadFile extends MainPage {
    get header() {
        return $('h3');
    }
    
    get link_1(){
        return $('a:nth-child(2)')
    }
    open(){
        return super.open('download');
    }
    async link_1_clk(){
        await this.link_1.click()
    }
}
module.exports = new DownloadFile