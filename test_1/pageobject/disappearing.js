const MainPage = require ('./main_page')

class Dis extends MainPage {
    get header() {
        return $('h3');
    }
    get home_btn() {
        return $('li:nth-child(1) > a')
    }
    get about_btn() {
        return $('li:nth-child(2) > a')
    }
    get contact_btn() {
        return $('li:nth-child(3) > a')
    }
    get portfolio_btn() {
        return $('li:nth-child(4) > a')
    }
    get gallery_btn() {
        return $('li:nth-child(5) > a')
    }

    open(){
        return super.open('disappearing_elements');
    }

    async home_click(){
        this.home_btn.click()
    }

    async about_click(){
        this.about_btn.click()
    }

    async contact_click(){
        this.contact_btn.click()
    }

    async portfolio_click(){
        this.portfolio_btn.click()
    }
    
    async gallery_click(){
        this.gallery_btn.click()
    }
}
module.exports = new Dis