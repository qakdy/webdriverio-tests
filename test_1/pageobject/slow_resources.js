const MainPage = require ('./main_page')

class Slow_resources extends MainPage {
    get header() {
        return $('h3');
    }

    open_1(){
        return super.open('slow');
    }

    open_2(){
        return super.open('slow_external');
    }
}
module.exports = new Slow_resources