const MainPage = require ('./main_page')

class Typos extends MainPage {

    get text(){
        return $('#content > div > p:nth-child(3)')
    }
    open(){
        return super.open('typos');
    }
}
module.exports = new Typos