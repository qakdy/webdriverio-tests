const MainPage = require ('./main_page')

class MultiPlayWindows extends MainPage {
    get header_new() {
        return $('h3');
    }
    
    get link_clk_here(){
        return $('#content > div > a')
    }

    async link_click(){
        await this.link_clk_here.click();
    }

    open(){
        return super.open('windows');
    }
}
module.exports = new MultiPlayWindows