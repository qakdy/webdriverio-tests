const MainPage = require ('./main_page')

class AlertPage extends MainPage{

    get result(){
        return $('#result')
    }

    getResultText(){
        return this.result.getText()
    }

    getAlertButton(index){
        return $(`ul li:nth-child(${index} button)`)
    }

    async getResultText(){
        return this.result.getText()
    }

    async clickOnAlertButton(index){
        this.getAlertButton(index).waitForDisplayed();
        this.getAlertButton(index).click();
    }
    

    open(){
        return super.open('javascript_alerts');
    } 
}

module.exports = new  AlertPage()