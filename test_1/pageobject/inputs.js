const MainPage = require ('./main_page')

class Inputs extends MainPage {
    get header() {
        return $('h3');
    }
    
    get input(){
        return $('input[type=number]')
    }

    open(){
        return super.open('inputs');
    }
}
module.exports = new Inputs