const MainPage = require ('./main_page')

class Jquery extends MainPage {
    get header() {
        return $('h3');
    }
    get site_link(){
        return $('.description > p:nth-child(1) > a')
    }

    get menu_enable(){
        return $('#ui-id-3 > a')
    }

    get back(){
        return $('#ui-id-8 > a')
    }

    get menu(){
        return $('#content > div > div > ul > li > a')
    }

    async site_link_click(){
        (await this.site_link).click()
    }
    open(){
        return super.open('jqueryui/menu');
    }
}
module.exports = new Jquery