const MainPage = require ('./main_page')

class Dom extends MainPage {
    get header() {
        return $('h3');
    }
    get all_dom(){
        return $('//*[@id="siblings"]');
    }
    get text_30_2(){
        return $('//*[@id="sibling-30.2"]')
    }
    get text_50_3(){
        return $('//*[@id="sibling-50.3"]')
    }
    open(){
        return super.open('large');
    }
}
module.exports = new Dom

