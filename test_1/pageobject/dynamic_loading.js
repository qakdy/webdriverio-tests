const MainPage = require ('./main_page')

class DynamicLoad extends MainPage {
    get header() {
        return $('h3');
    }

    get header_ex1(){
        return $('h4');
    }

    get ex_1_link(){
        return $('a:nth-child(5)')
    }

    get ex_2_link(){
        return $('a:nth-child(8)')
    }

    get finish(){
        return $('#finish')
    }

    get button_start(){
        return $('#start > button')
    }
    async ex1_clk(){
        await this.ex_1_link.click()
    }

    async ex2_clk(){
        await this.ex_2_link.click()
    }
    

    async button_start_clk(){
        await this.button_start.click()
    }
    
    open(){
        return super.open('dynamic_loading');
    }
}
module.exports = new DynamicLoad