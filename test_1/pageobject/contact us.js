const MainPage = require ('./main_page')

class Contact extends MainPage {
    get header() {
        return $('h1');
    }
    
    open(){
        return super.open('contact-us/');
    }
}
module.exports = new Contact