const MainPage = require ('./main_page')

class Main extends MainPage {
    get header() {
        return $('h1');
    }
    
    open(){
        return super.open('/');
    }
}
module.exports = new Main