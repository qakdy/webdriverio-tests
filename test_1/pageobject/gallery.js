const MainPage = require ('./main_page')

class Gallery extends MainPage {
    get header() {
        return $('h1');
    }
    
    open(){
        return super.open('gallery/');
    }
}
module.exports = new Gallery