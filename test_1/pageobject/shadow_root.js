const MainPage = require ('./main_page')

class ShadowRoot extends MainPage {
   
    open(){
        return super.open('shadowdom');
    }
}
module.exports = new ShadowRoot