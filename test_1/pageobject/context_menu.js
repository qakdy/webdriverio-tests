const MainPage = require ('./main_page')

class Context_Menu extends MainPage {
    get header() {
        return $('h3');
    }
    
    get right_clk(){
        return $('#hot-spot')
    }

    async clk() {
        await this.right_clk.click({ button: 'right' })
    }

    open(){
        return super.open('context_menu');
    }
}
module.exports = new Context_Menu