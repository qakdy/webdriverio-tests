const MainPage = require ('./main_page')

class KeyPress extends MainPage {
    get header() {
        return $('h3');
    }
    
    get input(){
        return $('#target')
    }
    get p_result(){
        return $('#result')
    }

    async input_click(){
        await this.p_result.click();
    }

    open(){
        return super.open('key_presses');
    }
}
module.exports = new KeyPress