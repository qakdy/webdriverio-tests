const MainPage = require ('./main_page')

class EntryAd extends MainPage {
    get header() {
        return $('h3');
    }
    get header_window() {
        return $('#modal > div.modal > div.modal-title');
    }

    get clk_here(){
        return $('#restart-ad')
    }
    get close(){
        return $('div.modal-footer > p')
    }
    async clk_hr(){
        await this.clk_here.click()
    }
    async close_clk(){
        await this.close.click()
    }
    open(){
        return super.open('entry_ad');
    }
}
module.exports = new EntryAd