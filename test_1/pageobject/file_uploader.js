const MainPage = require ('./main_page')

class FileUploader extends MainPage {
    get header() {
        return $('h3');
    }

    get new_header(){
        return $('h1')
    }

    get input(){
        return $('#file-upload');
    }

    get upload(){
        return $('#file-submit');
    }

    get result(){
        return $('#uploaded-files');
    }


    open(){
        return super.open('upload');
    }

    async upload_clk(){
        await this.upload.click();
    }
}
module.exports = new FileUploader