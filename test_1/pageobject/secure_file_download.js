const Page_Alert = require ('./page_alert')

class Secure extends Page_Alert {
    get header() {
        return $('h3');
    }

    open(){
        return super.open('https://admin:admin@the-internet.herokuapp.com/download_secure');
    }
}
module.exports = new Secure