const MainPage = require ('./main_page')

class Code extends MainPage {
    get header() {
        return $('h3');
    }
    get link_redirect(){
        return $('#redirect')
    }
    get link_status_codes(){
        return $('//*[@href="/status_codes"]')
    }
    get link_200(){
        return $('//*[@href="status_codes/200"]')
    }
    get link_301(){
        return $('//*[@href="status_codes/301"]')
    }
    get link_404(){
        return $('//*[@href="status_codes/404"]')
    }
    get link_500(){
        return $('//*[@href="status_codes/500"]')
    }
    async link_s_c_clk(){
        this.link_status_codes.click()
    }
    async link_redirect_clk(){
        this.link_redirect.click()
    }
    async link_200_clk(){
        this.link_200.click()
    }
    async link_301_clk(){
        this.link_301.click()
    }
    async link_404_clk(){
        this.link_404.click()
    }
    async link_500_clk(){
        this.link_500.click()
    }
    open(){
        return super.open('redirector');
    }
}
module.exports = new Code