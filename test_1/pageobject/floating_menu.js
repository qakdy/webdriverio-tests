const MainPage = require ('./main_page')

class FloatingMenu extends MainPage {
    get header() {
        return $('h3');
    }

    get home(){
        return $('li:nth-child(1) > a');
    }

    get news(){
        return $('li:nth-child(2) > a');
    }

    get contact(){
        return $('li:nth-child(3) > a')
    }

    get about(){
        return $('li:nth-child(4) > a')
    }

    get p2(){
        return $('p:nth-child(2)')
    }
    get p10(){
        return $('p:nth-child(10)')
    }

    get all_text(){
        return $$('p')
    }
    open(){
        return super.open('floating_menu');
    }

    async home_clk(){
        await this.home.click();
    }

    async news_clk(){
        await this.news.click();
    }

    async contact_clk(){
        await this.contact.click();
    }

    async about_clk(){
        await this.about.click();
    }

}
module.exports = new FloatingMenu