const MainPage = require ('./main_page')

class Frames extends MainPage {
    get header() {
        return $('h3');
    }
    get nested_frames(){
        return $('#content > div > ul > li:nth-child(1) > a')
    }
    get iframe(){
        return $('#content > div > ul > li:nth-child(2) > a')
    }
    get file_btn(){
        return $('div.tox-menubar > button:nth-child(1)')
    }
    get new_doc_btn(){
        return $('div.tox-collection__group')
    }
    get text_iframe(){
        return $('#tinymce > p')
    }
    get arrow_forward_btn(){
        return $('.tox-tbtn')
    }
    get arrow_back_btn(){
        return $('.tox-tbtn.tox-tbtn--disabled')
    }

    

    async new_doc_btn_clk(){
        this.new_doc_btn.click()
    }
    async file_btn_clk(){
        this.file_btn.click()
    }
    async nested_frames_clk(){
        this.nested_frames.click()
    }
    async iframe_clk(){
        this.iframe.click()
    }
    async arrow_back_btn_clk(){
        this.arrow_back_btn.click()
    }
    async arrow_forward_btn_clk(){
        this.arrow_forward_btn.click()
    }
    
    open(){
        return super.open('frames');
    }
}
module.exports = new Frames