const MainPage = require ('./main_page')

class Dropdown extends MainPage {
    get header() {
        return $('h3');
    }

    get dd(){
        return $('#dropdown')
    }

    get opt_1(){
        return $('option:nth-child(2)')
    }


    get opt_2(){
        return $('option:nth-child(3)')
    }

    async option_1(){
        await this.dd.selectByIndex(1) 
    }
    async dd_clk(){
        await this.dd.click()
    }
    async option_2(){
        await this.dd.selectByIndex(2) 
    }

    async option_1_atr(){
        await this.dd.selectByAttribute('value','1')
    }

    async option_2_atr(){
        await this.dd.selectByAttribute('value','2')
    }

    async option_1_text(){
        await this.dd.selectByVisibleText('Option 1')
    }

    async option_2_text(){
        await this.dd.selectByVisibleText('Option 2')
    }

    open(){
        return super.open('dropdown');
    }
}
module.exports = new Dropdown