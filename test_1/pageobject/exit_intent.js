const MainPage = require ('./main_page')

class Exit_Intent extends MainPage {
    get header() {
        return $('h3');
    }

    get up(){
        return $('#flash-messages');
    }
    get header_window() {
        return $('#ouibounce-modal > div.modal > div.modal-title');
    }

    get close(){
        return $('#ouibounce-modal > div.modal > div.modal-footer')
    }

    open(){
        return super.open('exit_intent');
    }

    async close_clk(){
        await this.close.click()
    }
}
module.exports = new Exit_Intent