const Page_Alert = require ('./page_alert')

class  Digest_Auth extends Page_Alert {
    get text() {
        return $('h3');
    }
    open(){
        return super.open('https://the-internet.herokuapp.com/digest_auth')
    }
    open_1(){
        return super.open('https://admin:admin@the-internet.herokuapp.com/digest_auth');
    }

    open_2 (){
        return super.open('https://user:admin@the-internet.herokuapp.com/digest_auth')
    }

    open_3 (){
        return super.open('https://admin:pass@the-internet.herokuapp.com/digest_auth')
    }
    
    open_4 (){
        return super.open('https://user:pass@the-internet.herokuapp.com/digest_auth')
    }
}
module.exports = new Digest_Auth