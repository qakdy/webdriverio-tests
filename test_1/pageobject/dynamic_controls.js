const MainPage = require ('./main_page')

class Dynamic_control extends MainPage {
    get header() {
        return $('h4');
    }
    get checkbox(){
        return $('input[type=checkbox]');
    }
    get rm_btn(){
        return $('#checkbox-example > button');
    }
    
    get enable_btn(){
        return $('#input-example > button');
    }

    get message_rm(){
        return $('#message');
    }

    get message_en(){
        return $('form#input-example > #message')
    }
    
    open(){
        return super.open('dynamic_controls');
    }
    async checkbox_clk(){
        this.checkbox.click();
    }

    async rm_btn_clk(){
        this.rm_btn.click();
    }

    async enable_clk(){
        this.enable_btn.click();
    }


}
module.exports = new Dynamic_control