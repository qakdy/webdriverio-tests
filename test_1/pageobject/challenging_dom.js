const MainPage = require ('./main_page')

class Challenging_dom extends MainPage {
    get header() {
        return $('h3');
    }
    

    get btn(){
        return $('a.button');
    }

    get btn_alert(){
        return $('a.button.alert');
    }

    get btn_scs(){
        return $('a.button.success');
    }
    
    get edit(){
        return $('td:nth-child(7) > a:nth-child(1)');
    }

    get delete(){
        return $('td:nth-child(7) > a:nth-child(2)');
    }
    
    get canvas(){
        return $('#canvas');
    }
    
    async edit_clk(){
        await this.edit.click();
    }

    async delete_clk(){
        await this.delete.click();
    }

    async all_clk(){
        await this.btn.click();
        await this.btn_alert.click();
        await this.btn_scs.click();

    }

    async btn_clk(){
        await this.btn.click();
    }
    async btn_alert_clk(){
        await this.btn_alert.click();
    }

    async btn_scs_clk(){
        await this.btn_scs.click();
    }

    async btn_atr(){
        await this.btn.getAttribute('class');
    }
    async btn_alert_atr(){
        await this.btn.getAttribute('class');
    }
    async btn_scs_atr(){
        await this.btn.getAttribute('class');
    }


    open(){
        return super.open('challenging_dom');
    }
}
module.exports = new Challenging_dom