const MainPage = require ('./main_page')

class Portfolio extends MainPage {
    get header() {
        return $('h1');
    }
    
    open(){
        return super.open('portfolio/');
    }
}
module.exports = new Portfolio