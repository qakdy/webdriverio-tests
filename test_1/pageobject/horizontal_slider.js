const MainPage = require ('./main_page')

class HorizonatalSlider extends MainPage {
    get header() {
        return $('h3');
    }
    
    get input_slider(){
        return $('input[type=range]')
    }
    get range(){
        return $('#range')
    }

    open(){
        return super.open('horizontal_slider');
    }
}
module.exports = new HorizonatalSlider