const MainPage = require ('./main_page')

class Geo extends MainPage {
    get header() {
        return $('h3');
    }

    get button(){
        return $('button')
    }

    get map_link(){
        return $('#map-link > a')
    }
    get latitude(){
        return $('#lat-value')
    }

    get longitude(){
        return $('#long-value')
    }
    
    get google_parameters(){
        return $('div.x3AX1-LfntMc-header-title-ij8cu > h2 > span')
    }
    async btn_clk(){
        this.button.click()
    }
    async map_link_clk(){
        this.map_link.click()
    }
    open(){
        return super.open('geolocation');
    }
}
module.exports = new Geo