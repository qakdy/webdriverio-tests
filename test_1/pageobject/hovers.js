const MainPage = require ('./main_page')

class Hovers extends MainPage {
    get header() {
        return $('h3');
    }
    
    get header_img1(){
        return $('.example>div:nth-child(3)>.figcaption>h5')
    }
    get link_img1(){
        return $('.example>div:nth-child(3)>.figcaption>a')
    }
    
    get header_img2(){
        return $('.example>div:nth-child(4) > .figcaption > h5')
    }
    get link_img2(){
        return $('.example>div:nth-child(4) > .figcaption> a')
    }
    
    get header_img3(){
        return $('.example>div:nth-child(5) > .figcaption > h5')
    }
    get link_img3(){
        return $('.example>div:nth-child(5) > .figcaption> a')
    }

    
    async link_img1_clk(){
        this.link_img1.click()
    }
    async link_img2_clk(){
        this.link_img2.click()
    }
    async link_img3_clk(){
        this.link_img3.click()
    }
    open(){
        return super.open('hovers');
    }
}
module.exports = new Hovers
