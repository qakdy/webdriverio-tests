const MainPage = require ('./main_page')

class Checboxes extends MainPage {
    get header() {
        return $('h3');
    }
    
    get chk_1() {
        return $('input:nth-child(1)');
    }

    get chk_2() {
        return $('input:nth-child(3)');
    }

    get canvas(){
        return $('#canvas');
    }

    async chk_clk_1(){
        await this.chk_1.click()
    }

    async chk_clk_2(){
        await this.chk_2.click()
    }


    async chk_clk_all(){
        await this.chk_2.click()
        await this.chk_1.click()
    }

    open(){
        return super.open('checkboxes');
    }
}
module.exports = new Checboxes