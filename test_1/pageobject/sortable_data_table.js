const MainPage = require ('./main_page')

class Data_Tables extends MainPage {
    get header() {
        return $('h3');
    }


    get edit_2(){
        return $('#table2 > tbody > tr:nth-child(1) > td.action > a:nth-child(1)')
    }
    get delete_2(){
        return $('#table2 > tbody > tr:nth-child(1) > td.action > a:nth-child(2)')
    }
    get edit_1(){
        return $('#table1 > tbody > tr:nth-child(1) > td:nth-child(6) > a:nth-child(1)')
    }
    get delete_1(){
        return $('#table1 > tbody > tr:nth-child(1) > td:nth-child(6) > a:nth-child(2)')
    }
    open(){
        return super.open('tables');
    }
}
module.exports = new Data_Tables