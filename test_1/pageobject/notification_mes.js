const MainPage = require ('./main_page')

class NotificationMessage extends MainPage {
    get header() {
        return $('h3');
    }
    
    get link_clk_here(){
        return $('#content > div > p > a')
    }

    get not_mes(){
        return $('#flash')
    }

    async link_click(){
        await this.link_clk_here.click();
    }
    

    open(){
        return super.open('notification_message_rendered');
    }
}
module.exports = new NotificationMessage