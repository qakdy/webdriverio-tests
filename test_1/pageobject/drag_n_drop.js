const MainPage = require ('./main_page')

class Drad_and_drop extends MainPage {
    get header() {
        return $('h3');
    }
    
    get columnA(){
        return $('#column-a')
    }
    
    get columnAHeader() { 
        return this.columnA.$('header'); 
    }

    get columnB(){
        return $('#column-b')
    }

    open(){
        return super.open('drag_and_drop');
    }
}
module.exports = new Drad_and_drop