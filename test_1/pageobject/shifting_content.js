const MainPage = require ('./main_page')

class ShiftingContent extends MainPage {
   
    get ex_1(){
        return $('#content > div > a:nth-child(3)')
    }

    get ex_2(){
        return $('#content > div > a:nth-child(6)')
    }

    get ex_3(){
        return $('#content > div > a:nth-child(9)')
    }
    get home(){
        return $('#content > div > ul > li:nth-child(1) > a')
    }
    get about(){
        return $('#content > div > ul > li:nth-child(2) > a')
    }
    get contact_us(){
        return $('#content > div > ul > li:nth-child(3) > a')
    }
    get portfolio(){
        return $('#content > div > ul > li:nth-child(4) > a')
    }
    get gallery(){
        return $('#content > div > ul > li:nth-child(5) > a');
    }
    
    get clk_random(){
        return $('#content > div > p:nth-child(3) > a')
    }

    get clk_shift_100(){
        return $('#content > div > p:nth-child(4) > a')
    }

    get clk_together(){
        return $('#content > div > p:nth-child(5) > a')
    }

    get image_changed(){
        return $('#content > div > p:nth-child(6) > a')
    }
    
    get image(){
        return $('.shift')
    }
    
    get ex_3_text(){
        return $('div.large-6')
    }
    open(){
        return super.open('shifting_content');
    }
}
module.exports = new ShiftingContent
