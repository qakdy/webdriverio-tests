const MainPage = require ('./main_page')

class Add_Remove extends MainPage {
    get header() {
        return $('h3');
    }

    get add_button() {
        return $('button')
    }

    get buttons() {
        return $$('button')
    }

    get bnt_delete() {
        return $('button.added-manually')
    }

    async button_clk () {
        await this.add_button.click();
    }

    async wait_buttons(){
        await this.buttons[1].waitForDisplayed()
    }

    open(){
        return super.open('add_remove_elements/');
    }
}
module.exports = new Add_Remove