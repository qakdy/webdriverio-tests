const MainPage = require ('./main_page')

class Abtest extends MainPage {
    get header() {
        return $('h3');
    }

    open(){
        return super.open('abtest');
    }
}
module.exports = new Abtest