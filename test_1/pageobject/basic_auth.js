const Page_Alert = require ('./page_alert')

class  Basic_Auth extends Page_Alert {
    get text() {
        return $('p');
    }

    get body_text() {
        return $('body');
    }
    open(){
        return super.open('https://the-internet.herokuapp.com/basic_auth')
    }
    open_1(){
        return super.open('https://admin:admin@the-internet.herokuapp.com/basic_auth');
    }


    open_2 (){
        return super.open('https://user:admin@the-internet.herokuapp.com/basic_auth')
    }

    open_3 (){
        return super.open('https://admin:pass@the-internet.herokuapp.com/basic_auth')
    }
    
    open_4 (){
        return super.open('https://user:pass@the-internet.herokuapp.com/basic_auth')
    }
}
module.exports = new Basic_Auth
