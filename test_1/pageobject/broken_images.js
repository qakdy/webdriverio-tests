const MainPage = require ('./main_page')

class Broken extends MainPage {
    get header() {
        return $('h3');
    }
    
    get photo_broken_1(){
        return $('img:nth-child(2)');
    }

    get photo_broken_2(){
        return $('img:nth-child(3)');
    }
    
    get photos(){
        return $('img:nth-child(4)');
    }

    open(){
        return super.open('broken_images');
    }

    // async ph_br1_atr(){
    //     this.photo_broken_1.getAttribute('src')
    // }
    
}
module.exports = new Broken