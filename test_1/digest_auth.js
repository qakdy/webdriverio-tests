const Digest_Authentication = require('./pageobject/digest_auth.js')


describe('Digest Authentication', () => {
    it('Digest Authentication test_2, should login in alert and check text in paragraph', async () => {
        await Digest_Authentication.open_1();
        await expect(Digest_Authentication.text).toHaveTextContaining('Digest Auth');
    });

    it('basic auth test_3, should not login in alert, because user and pass --- user:admin', async () => {
        await Digest_Authentication.open_2();
        await expect(Digest_Authentication.text).toBeUndefined;
    });

});
