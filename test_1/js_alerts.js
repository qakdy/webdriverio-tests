const AlertPage = require('./pageobject/js_alerts')


describe('floating menu test', () => {
    it('should open page and accept alert,', async () => {
        await AlertPage.open();
        await AlertPage.clickOnAlertButton(1);
        const alert_text = await browser.getAlertText();
        await expect(alert_text).toHaveTextContaining('I am a JS Alert');
        await browser.acceptAlert();
        await expect(AlertPage.getResultText()).toHaveTextContaining('You successfully clicked an alert');
    });
    it('should dissmiss alert,', async () => {
        await AlertPage.open();
        await AlertPage.clickOnAlertButton(2);
        await browser.dismissAlert();
        await expect(AlertPage.getResultText()).toHaveTextContaining('You clicked: Cancel');
    });
    it('should send text in alert,', async () => {
        await AlertPage.open();
        await AlertPage.clickOnAlertButton(3);
        await browser.sendAlertText('hello world');
        await browser.acceptAlert();
        await expect(AlertPage.getResultText()).toHaveTextContaining('You entered: hello world');
    });
});
