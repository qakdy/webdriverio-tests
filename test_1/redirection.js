const redirect = require('./pageobject/redirection')

describe('redirect tests', () => {
    it('open page, check url, clk redirection link and check new url   ', async () => {
        await redirect.open();
        await expect(browser).toHaveUrlContaining('https://the-internet.herokuapp.com/redirector')
        
        await redirect.redirection_link.click();
        await expect(browser).toHaveUrlContaining('https://the-internet.herokuapp.com/status_codes')
    });
});