const Tables = require('./pageobject/sortable_data_table');

describe('sortable data tables test', () => {
    it('should open page and check header in example 1', async () => {
        await Tables.open();
        const header_1 = await $$('#table1 > thead > tr > th.header')
        for(var i= 0; i < header_1.length; i++){
            const array_h_1 = ['Last Name','First Name','Email','Due','Web Site','Action']
            await expect(header_1[i]).toHaveTextContaining(array_h_1[i])
        };
    });

    it('check text in example_1', async () => {
        await Tables.open();
        const tr = await $$('#table1 > tbody > tr > td')
        const array_tr = ['Smith','John','jsmith@gmail.com','$50.00','http://www.jsmith.com','edit delete',
        'Bach','Frank','fbach@yahoo.com','$51.00','http://www.frank.com','edit delete',
        'Doe','Jason','jdoe@hotmail.com','$100.00','http://www.jdoe.com','edit delete',
        'Conway','Tim','tconway@earthlink.net','$50.00','http://www.timconway.com','edit delete']
        for(var i= 0; i < tr.length; i++){
            await expect(tr[i]).toHaveTextContaining(array_tr[i])
        };
    });

    it('example 1(click edit and delete) and check changes in URL', async () => {
        await Tables.edit_1.click()
        await expect(browser).toHaveUrlContaining('https://the-internet.herokuapp.com/tables#edit')
        await Tables.delete_1.click()
        await expect(browser).toHaveUrlContaining('https://the-internet.herokuapp.com/tables#delete')
        
    });

    it('check that web site link is clickable in example 1(Web Site)', async () => {
        const link_1 = await $('#table1 > tbody > tr:nth-child(1) > td:nth-child(5)').getAttribute('href');
        await expect(link_1).toBeNull()
        const link_2 = await $('#table1 > tbody > tr:nth-child(2) > td:nth-child(5)').getAttribute('href');
        await expect(link_2).toBeNull()
        const link_3 = await $('#table1 > tbody > tr:nth-child(3) > td:nth-child(5)').getAttribute('href');
        await expect(link_3).toBeNull()
        const link_4 = await $('#table1 > tbody > tr:nth-child(4) > td:nth-child(5)').getAttribute('href');
        await expect(link_4).toBeNull()
    });

    it('check text in example 2(Last Name)', async () => {
        const last_name = await $$('.last-name')
        const array_l = ["Last Name","Smith","Bach","Doe","Conway"]
        for(var i= 0; i < last_name.length; i++){
            await expect(last_name[i]).toHaveTextContaining(array_l[i])
        };
    });

    it('check text in example 2(First Name)', async () => {
        const first_name = await $$('.first-name')
        const array_f = ["First Name","John","Frank","Jason","Tim"]
        for(var i= 0; i < first_name.length; i++){
            await expect(first_name[i]).toHaveTextContaining(array_f[i])
        };
    });

    it('check text in example 2(Email)', async () => {
        const email = await $$('.email')
        const array_e = ["Email","jsmith@gmail.com","fbach@yahoo.com","jdoe@hotmail.com","tconway@earthlink.net"]
        for(var i= 0; i < email.length; i++){
            await expect(email[i]).toHaveTextContaining(array_e[i])
        };
    });

    it('check text in example 2(Due)', async () => {
        const dues = await $$('.dues')
        const array_d = ["Due","$50.00","$51.00","$100.00","$50.00"]
        for(var i= 0; i < dues.length; i++){
            await expect(dues[i]).toHaveTextContaining(array_d[i])
        };
    });

    it('check text and example 2(Web_Site)', async () => {
        const web = await $$('.web-site')
        const array_w = ["Web Site","http://www.jsmith.com","http://www.frank.com","http://www.jdoe.com","http://www.timconway.com"]
        for(var i= 0; i < web.length; i++){
            await expect(web[i]).toHaveTextContaining(array_w[i])
        };
    });

    it('check text and example 2(Action)', async () => {
        const action = await $$('.action')
        const array_a = ["Action","edit delete","edit delete","edit delete","edit delete"]
        for(var i= 0; i < action.length; i++){
            await expect(action[i]).toHaveTextContaining(array_a)
        };
    });

    it('check clicable on buttonss example 2(Action)', async () => {
        const action = await $$('td.action>a')
        for(var i= 0; i < action.length; i++){
            await expect(action[i]).toBeClickable();
        };
    });

    it('example 2(click edit and delete) and check changes in URL', async () => {
        await Tables.edit_2.click()
        await expect(browser).toHaveUrlContaining('https://the-internet.herokuapp.com/tables#edit')
        await Tables.delete_2.click()
        await expect(browser).toHaveUrlContaining('https://the-internet.herokuapp.com/tables#delete')
        
    });
    
    it('check text in example 2(header)', async () => {
        const header = await $$('#table2 > thead > tr > th.header')
        for(var i= 0; i < header.length; i++){
            const array_h = ['Last Name','First Name','Email','Due','Web Site','Action']
            await expect(header[i]).toHaveTextContaining(array_h[i])
        };
    });

    it('check that web site link is clickable in example 2(Web Site)', async () => {
        const Web_Site = await $$('td.web-site')
        for(var i= 0; i < Web_Site.length; i++){
            var link = await Web_Site[i].getAttribute('href')
            await expect(link).toBeNull()
        };
    });
});