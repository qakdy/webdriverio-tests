const Jquery = require('./pageobject/jquery_ui_menu')

describe('jquery ui test', () => {
    it('open page , click on link and check new url', async () => {
        await Jquery.open();
        await Jquery.site_link_click();
        await expect(browser).toHaveUrl('https://api.jqueryui.com/menu/')
    })
    it('open page , click on menu(enable) and check  url', async () => {
        await Jquery.open();
        await expect(browser).toHaveUrl('https://the-internet.herokuapp.com/jqueryui/menu')
        await Jquery.menu_enable.click();
        await expect(browser).not.toHaveUrl('https://the-internet.herokuapp.com/jqueryui/menu')
    })
    
})