const HorizonatalSlider = require('./pageobject/horizontal_slider');

describe('floating menu test', () => {
    it('should open page and check range,', async () => {
        await HorizonatalSlider.open();
        await expect(HorizonatalSlider.range).toHaveTextContaining('0');
    });
    it('should open page and check range,', async () => {
        await HorizonatalSlider.input_slider.click();
        await browser.keys("ArrowUp");
        await expect(HorizonatalSlider.range).toHaveTextContaining('3');
    });
    it('should open page and check range,', async () => {
        await browser.refresh();
        await expect(HorizonatalSlider.range).toHaveTextContaining('0');
    });
    xit('use drag n drop ,', async () => {
        await browser.maximizeWindow();
        const el = await HorizonatalSlider.range.getLocation();
        console.log('````````````````````````',el)
        await HorizonatalSlider.input_slider.click({x:377, y:165})
        await expect(HorizonatalSlider.range).toHaveTextContaining('1');
    });
});