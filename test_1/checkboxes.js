const Checboxes =  require('./pageobject/checkboxes')

describe('checkboxes', () => {
    it('open page and check text in header and selected elements', async () => {
        await Checboxes.open();
        await expect(Checboxes.header).toHaveTextContaining('Checkboxes');
        await expect(Checboxes.chk_2).toBeChecked();
    });
    it('click on checkbox 2 and check checkbox 1 and 2 not to be checked', async () => {
        await Checboxes.chk_clk_2();
        await expect(Checboxes.chk_1).not.toBeChecked();
        await expect(Checboxes.chk_2).not.toBeChecked();
    });
    it('click on checkbox 1 and 2, all should be selected', async () => {
        await Checboxes.chk_clk_all();
        await expect(Checboxes.chk_1).toBeChecked();
        await expect(Checboxes.chk_2).toBeChecked();
    });
    it('refresh page, checbox-1 - not to be checked; checkbox-2 -to be checked', async () => {
        await browser.refresh();
        await expect(Checboxes.chk_1).not.toBeChecked();
        await expect(Checboxes.chk_2).toBeChecked();
    });
    it('click on checkbox 1 , 1 checkbox - to be selected, 2 checkbox - selected', async () => {
        await Checboxes.chk_clk_1();
        await expect(Checboxes.chk_1).toBeChecked();
        await expect(Checboxes.chk_2).toBeChecked();
    });
});
