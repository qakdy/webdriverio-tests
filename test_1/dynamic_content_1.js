describe('Dynamic Content', () => {
    it('should open and check text header "Dynamic content"', async () => {
        await browser.url('https://the-internet.herokuapp.com/dynamic_content');
        await expect($('h3')).toHaveTextContaining('Dynamic Content'); 
    });

    it('check link to be clickable', async () => {
        await expect($('p:nth-child(3) > a')).toBeClickable();
        });

    it('check changes in src in img_1 and text_1 with browser refresh ', async () => {
        const src_1 = await $('div:nth-child(1) > div.large-2.columns > img').getAttribute('src');
        const text_1 = await $('div:nth-child(1) > div.large-10.columns').getText();
        await browser.refresh();
        const src_1_changed = await $('div:nth-child(1) > div.large-2.columns > img').getAttribute('src');
        const text_1_changed = await $('div:nth-child(1) > div.large-10.columns').getText();
        await expect(src_1).not.toBe(src_1_changed);
        await expect(text_1).not.toBe(text_1_changed);
    });

    it('check changes in src in img_2 and text_2 with browser refresh ', async () => {  
        const src_2 = await $('div:nth-child(4) > div.large-2.columns > img').getAttribute('src');
        const text_2 = await $('div:nth-child(4) > div.large-10.columns').getText();
        await browser.refresh();
        const src_2_changed = await $('div:nth-child(4) > div.large-2.columns > img').getAttribute('src');
        const text_2_changed = await $('div:nth-child(4) > div.large-10.columns').getText();
        await expect(src_2).not.toBe(src_2_changed);
        await expect(text_2).not.toBe(text_2_changed);
    });

    it('check changes in src in img_3 and text_3 with browser refresh ', async () => {  
        const src_3 = await $('div:nth-child(7) > div.large-2.columns > img').getAttribute('src');
        const text_3 = await $('div:nth-child(7) > div.large-10.columns').getText();
        await browser.refresh();
        const src_3_changed = await $('div:nth-child(7) > div.large-2.columns > img').getAttribute('src');
        const text_3_changed = await $('div:nth-child(7) > div.large-10.columns').getText();
        await expect(src_3).not.toBe(src_3_changed);
        await expect(text_3).not.toBe(text_3_changed);
    });

    // click link "click here" for the first time ,shold change all img, text and change url title


    it('check changes in src in img_1,2,3 and text_1,2,3 with click link for the first time ', async () => {
        await browser.url('https://the-internet.herokuapp.com/dynamic_content');

        const src_1_link = await $('div:nth-child(1) > div.large-2.columns > img').getAttribute('src');
        const text_1_link = await $('div:nth-child(1) > div.large-10.columns').getText();
        const src_2_link = await $('div:nth-child(4) > div.large-2.columns > img').getAttribute('src');
        const text_2_link = await $('div:nth-child(4) > div.large-10.columns').getText();
        const src_3_link = await $('div:nth-child(7) > div.large-2.columns > img').getAttribute('src');
        const text_3_link = await $('div:nth-child(7) > div.large-10.columns').getText();

        await $('p:nth-child(3) > a').click();
        await expect(browser).toHaveUrl('https://the-internet.herokuapp.com/dynamic_content?with_content=static');
    
        const src_1_link_changed = await $('div:nth-child(1) > div.large-2.columns > img').getAttribute('src');
        const text_1_link_changed = await $('div:nth-child(1) > div.large-10.columns').getText();
        const src_2_link_changed = await $('div:nth-child(4) > div.large-2.columns > img').getAttribute('src');
        const text_2_link_changed = await $('div:nth-child(4) > div.large-10.columns').getText();
        const src_3_link_changed = await $('div:nth-child(7) > div.large-2.columns > img').getAttribute('src');
        const text_3_link_changed = await $('div:nth-child(7) > div.large-10.columns').getText();
        await expect(src_1_link).not.toBe(src_1_link_changed);
        await expect(text_1_link).not.toBe(text_1_link_changed);
        await expect(src_2_link).not.toBe(src_2_link_changed);
        await expect(text_2_link).not.toBe(text_2_link_changed);
        await expect(src_3_link).not.toBe(src_3_link_changed);
        await expect(text_3_link).not.toBe(text_3_link_changed);
    });
    //click link the second time
    it('check changes in src in img_1 and text_1 with click link for the second  time ', async () => {
        const src_1_link = await $('div:nth-child(1) > div.large-2.columns > img').getAttribute('src');
        const text_1_link = await $('div:nth-child(1) > div.large-10.columns').getText();

        await $('p:nth-child(3) > a').click();
        await expect(browser).toHaveUrl('https://the-internet.herokuapp.com/dynamic_content?with_content=static');
        
        const src_1_link_changed = await $('div:nth-child(1) > div.large-2.columns > img').getAttribute('src');
        const text_1_link_changed = await $('div:nth-child(1) > div.large-10.columns').getText();

        await expect(src_1_link).toBe(src_1_link_changed);
        await expect(text_1_link).toBe(text_1_link_changed);
    });

    it('check changes in src in img_2 and text_2 with click link for the second  time ', async () => {
        const src_2_link = await $('div:nth-child(4) > div.large-2.columns > img').getAttribute('src');
        const text_2_link = await $('div:nth-child(4) > div.large-10.columns').getText();

        await $('p:nth-child(3) > a').click();
        await expect(browser).toHaveUrl('https://the-internet.herokuapp.com/dynamic_content?with_content=static');
        
        const src_2_link_changed = await $('div:nth-child(4) > div.large-2.columns > img').getAttribute('src');
        const text_2_link_changed = await $('div:nth-child(4) > div.large-10.columns').getText();

        await expect(src_2_link).toBe(src_2_link_changed);
        await expect(text_2_link).toBe(text_2_link_changed);
    });
    
    it('check changes in src in text_3 with click link for the second  time ', async () => {
        const text_3_link = await $('div:nth-child(7) > div.large-10.columns').getText();

        await $('p:nth-child(3) > a').click();
        await expect(browser).toHaveUrl('https://the-internet.herokuapp.com/dynamic_content?with_content=static');
    
        const text_3_link_changed = await $('div:nth-child(7) > div.large-10.columns').getText();

        await expect(text_3_link).not.toBe(text_3_link_changed);
    });
        
});
