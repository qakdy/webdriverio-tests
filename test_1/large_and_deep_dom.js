const Dom = require('./pageobject/large_and_deep_dom');

describe('deep and large dom tests', () => {
    it('open page;scroll to the element and check text ',async () => {
        await Dom.open();
        await Dom.text_30_2.scrollIntoView();
        await expect(Dom.text_30_2).toHaveText('30.2')
        await expect(Dom.text_30_2).toBeDisplayedInViewport();
    });
    it('open page;scroll to the last element and check text ',async () => {
        await Dom.open();
        await Dom.text_50_3.scrollIntoView();
        await expect(Dom.text_50_3).toHaveText('50.3')
        await expect(Dom.text_50_3).toBeDisplayedInViewport();
    });
    it('table td_cloumn1 check ',async () => {
        const td_column1 = await $$('td.column-1')
        for(var i= 0; i< td_column1.length; i++){
            var x = i + 1;
            await expect(td_column1[i]).toHaveTextContaining([x]+".1")
        };
    });
    it('table td_cloumn2 check ',async () => {
        const td_column2 = await $$('td.column-2')
        for(var i= 0; i< td_column2.length; i++){
            var x = i + 1;
            await expect(td_column2[i]).toHaveTextContaining([x]+".2")
        };
    });
    it('table td_cloumn49 check ',async () => {
        const td_column49 = await $$('td.column-49')
        for(var i= 0; i< td_column49.length; i++){
            var x = i + 1;
            await expect(td_column49[i]).toHaveTextContaining([x]+".49")
        };
    });
    it('table td_cloumn50 check ',async () => {
        const td_column50 = await $$('td.column-50')
        for(var i= 0; i< td_column50.length; i++){
            var x = i + 1;
            await expect(td_column50[i]).toHaveTextContaining([x]+".50")
        };
    });
    it('table tr.row-1 check ',async () => {
        const tr_row1 = await $$('tr.row-1')
        for(var i= 0; i< tr_row1.length; i++){
            var x = i + 1;
            await expect(tr_row1[i]).toHaveTextContaining("1."+[x])
        };
    });
    it('table tr.row-2 check ',async () => {
        const tr_row2 = await $$('tr.row-2')
        for(var i= 0; i< tr_row2.length; i++){
            var x = i + 1;
            await expect(tr_row2[i]).toHaveTextContaining("2."+[x])
        };
    });
    it('table tr.row-49 check ',async () => {
        const tr_row49 = await $$('tr.row-49')
        for(var i= 0; i< tr_row49.length; i++){
            var x = i + 1;
            await expect(tr_row49[i]).toHaveTextContaining("49."+[x])
        };
    });
    it('table tr.row-50 check ',async () => {
        const tr_row50 = await $$('tr.row-50')
        for(var i= 0; i< tr_row50.length; i++){
            var x = i + 1;
            await expect(tr_row50[i]).toHaveTextContaining("50."+[x])
        };
    });
});