const Basic_Auth = require('./pageobject/basic_auth')


describe('basic_auth', () => {
    it('basic auth test_2, should login in alert and check text in paragraph', async () => {
        await Basic_Auth.open_1();
        await expect(Basic_Auth.text).toHaveTextContaining('Congratulations! You must have the proper credentials.');
    });
    it('basic auth test_3, should not login in alert, because user and pass --- user:admin', async () => {
        await Basic_Auth.open_2();
        await expect(Basic_Auth.text).toBeUndefined;
    });

    it('basic auth test_4, should not login in alert, because user and pass --- admin:pass ', async () => {
        await Basic_Auth.open_3();
        await expect(Basic_Auth.text).toBeUndefined;
    });

    it('basic auth test_5, should not login in alert, because user and pass --- user:pass', async () => {
        await Basic_Auth.open_4();
        await expect(Basic_Auth.text).toBeUndefined;
    });
});       