const Inputs = require('./pageobject/inputs');

describe('Inputs test', () => {
    it('should open page and add value "100000"', async () => {
        await Inputs.open();
        await Inputs.input.setValue(100000);
        const atr = await Inputs.input.getValue();
        await expect(atr).toEqual("100000");
    });
    it('should open page and add value  "-100000"', async () => {
        await Inputs.open();
        await Inputs.input.setValue(-100000);
        const atr_1= await Inputs.input.getValue();
        await expect(atr_1).toEqual("-100000");
    });
    it('should open page and add value "5.4"', async () => {
        await Inputs.open();
        await Inputs.input.setValue(5.4);
        const atr_2 = await Inputs.input.getValue();
        await expect(atr_2).toEqual("5.4");
    });
    it('should open page and use arrows', async () => {
        await Inputs.open();
        await Inputs.input.click();
        await browser.keys('ArrowDown');
        const atr_3 = await Inputs.input.getValue();
        await expect(atr_3).toEqual("-1");
        await browser.keys('ArrowUp');
        const atr_4 = await Inputs.input.getValue();
        await expect(atr_4).toEqual("0");
    });
    it('should open page and not add value "abcd"', async () => {
        await Inputs.open();
        await Inputs.input.setValue('abcd');
        const atr_5 = await Inputs.input.getValue();
        await expect(atr_5).not.toEqual("abcd");
    });
});