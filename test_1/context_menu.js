const Context_Menu = require('./pageobject/context_menu')

describe('context menu', () => {
    it('context menu test_1, check header "Context Menu"', async () => {
        await Context_Menu.open();
        await expect(Context_Menu.header).toHaveTextContaining('Context Menu');
    });
    it('alert check text and accept', async () => {
        await Context_Menu.clk();
        const alert_1 = await browser.getAlertText();
        await expect(alert_1).toEqual('You selected a context menu');
        await browser.acceptAlert();
    });
})
