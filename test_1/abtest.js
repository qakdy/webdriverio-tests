const AbtestPage = require ('./pageobject/abtest_page.js') 


describe('abtest', () => {
    it('should open abtest page and check text in header - "Test Variation 1" or "A/B Test Control"', async () => {
        AbtestPage.open()
        var text = await AbtestPage.header.getText();
        var opt_1 = 'A/B Test Variation 1'
        var opt_2 = 'A/B Test Control' 
        if (text !== opt_1){
            await expect(text).toBe(opt_2)
        }
    });
});