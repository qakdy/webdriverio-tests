
const Dropdown = require('./pageobject/dropdown')

describe('dropdown', () => {
    it('should open and check text in header', async () => {
        await Dropdown.open();
        await expect(Dropdown.header).toHaveTextContaining('Dropdown List');    
    });
    it('select option 1 by Index, to be 1', async () => {
        await Dropdown.option_1();
        const value_1 = await Dropdown.dd.getValue();
        await expect(value_1).toBe('1');
    });
    it('select option 2 by Index, to be selected and option 1 not selected', async () => {
        await Dropdown.option_2();
        await expect(Dropdown.opt_2).toBeSelected();
        await expect(Dropdown.opt_1).not.toBeSelected();
    });
    it('select option 1 by Atr, to be 1', async () => {
        await Dropdown.option_1_atr();
        const atr_1 = await Dropdown.dd.getValue();
        await expect(atr_1).toBe('1');
    });
    it('select option 2 by Atr, to be checked', async () => {
        await Dropdown.option_2_atr();
        await expect(Dropdown.opt_2).toBeChecked();

    });
    it('select option 1 by Text, to be selected', async () => {
        await Dropdown.option_1_text();
        await expect(Dropdown.opt_1).toBeSelected();
    });
    it('select option 2 by Text, check text and to be selected', async () => {
        await Dropdown.option_2_text();
        const text = await Dropdown.opt_2.getText();
        await expect(text).toBe('Option 2')
        await expect(Dropdown.opt_2).toBeSelected();
    });
    it('refresh page and check dropdown,  option_1 and option_2 shouldnt be selected ', async () => {
        await browser.refresh();
        await expect(Dropdown.opt_1).not.toBeSelected();
        await expect(Dropdown.opt_2).not.toBeSelected();
    });
    it('select option_1 , click on header and check that option_1 be selected and option_2 is not visible, and check dropdown form is not  enable', async () => {
        await Dropdown.option_1();
        const value_1 = await Dropdown.dd.getValue();
        await expect(value_1).toBe('1');
        await Dropdown.dd_clk();
        await Dropdown.header.click();
        await expect(Dropdown.opt_1).toBeSelected();
        await expect(Dropdown.opt_2).not.toBeDisplayedInViewport()
        await expect(Dropdown.dd).not.toBeDisabled()
    });
    

});