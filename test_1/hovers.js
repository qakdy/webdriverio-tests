const Hovers = require('./pageobject/hovers');

describe('Hovers test', () => {
    it('should open page check img header, click link under img1 and check new url ', async () => {
        await Hovers.open();
        await $$('.figure')[0].moveTo();
        await expect(Hovers.header_img1).toHaveTextContaining('name: user1')
        await Hovers.link_img1_clk();
        await expect(browser).toHaveUrl('https://the-internet.herokuapp.com/users/1')
    });
    it('should open page check img header, click link under img1 and check new url ', async () => {
        await Hovers.open();
        await $$('.figure')[1].moveTo();
        await expect(Hovers.header_img2).toHaveTextContaining('name: user2')
        await Hovers.link_img2_clk();
        await expect(browser).toHaveUrl('https://the-internet.herokuapp.com/users/2')
    });
    it('should open page check img header, click link under img1 and check new url ', async () => {
        await Hovers.open();
        await $$('.figure')[2].moveTo();
        await expect(Hovers.header_img3).toHaveTextContaining('name: user3')
        await Hovers.link_img3_clk();
        await expect(browser).toHaveUrl('https://the-internet.herokuapp.com/users/3')
    });
});
