const MultiPlayWindows = require('./pageobject/multiplay_windows');

describe('multiplay windows test', () => {
    it('should open page and check header,', async () => {
        await MultiPlayWindows.open();
        await expect(browser).toHaveUrl('https://the-internet.herokuapp.com/windows')
        await MultiPlayWindows.link_click();
        await browser.switchWindow('https://the-internet.herokuapp.com/windows/new');
        await expect(MultiPlayWindows.header_new).toHaveTextContaining('New Window')
    });
});