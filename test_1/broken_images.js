const Broken = require('./pageobject/broken_images')

describe('broken_images', () => {
    it('should open and check text header', async () => {
        await Broken.open();
        await expect(Broken.header).toHaveTextContaining('Broken Images');
    });
    it('should get atribute_1 and src dont have path with (img/), because img is broken', async () => {
        const atr = await Broken.photo_broken_1.getAttribute('src');
        await expect(atr).not.toBe('img/asdf.jpg');
    });
    it('should get atribute_2 and src dont have path with (img/), because img is broken', async () => {
        const atr2 = await Broken.photo_broken_2.getAttribute('src');
        await expect(atr2).not.toBe('img/hjkl.jpg');
    });
    it('should get atribute_3 and src have path with (img/)', async () => {
        const atr3 = await Broken.photos.getAttribute('src');
        await expect(atr3).toBe('img/avatar-blank.jpg');
    });
})
