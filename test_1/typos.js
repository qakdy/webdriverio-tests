const Typos = require('./pageobject/typos')


describe('typos tests', () => {
    it('open page and check changes in text ', async () => {
        await Typos.open();
        const text_2 = "Sometimes you"+"'"+"ll see a typo, other times you won"+"'"+"t."
        const text_1 = "Sometimes you"+"'"+"ll see a typo, other times you won"+","+"t."
        const text = await Typos.text.getText()
        if(text === text_1){
            await expect(text).not.toBe(text_2)
        }else{
            await expect(text).toBe(text_2)
        }
        
    });
});
