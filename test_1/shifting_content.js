const ShiftingContent = require('./pageobject/shifting_content')

describe('shifting content tests', () => {
    it('open page, clk ex_1, get location of button (gallery) and check location changes ', async () => {
        await ShiftingContent.open();
        await ShiftingContent.ex_1.click();
        await ShiftingContent.gallery.waitForDisplayed()
        const loc_1 = await ShiftingContent.gallery.getLocation();
        const xy = { x: 538.3671875, y: 293.5703125 }
        if (loc_1 !== xy){
            await browser.refresh();
            await ShiftingContent.gallery.waitForDisplayed()
            await expect(loc_1).toEqual(xy)
        } else{
            expect(loc_1).toEqual(xy)
        }
    });

    it(' check location changes with button (gallery), when browser refresh page', async () => {
        const loc_1 = await ShiftingContent.gallery.getLocation();
        await browser.refresh();
        const loc_2 = await ShiftingContent.gallery.getLocation();
        await expect(loc_1).not.toEqual(loc_2)
    });

    it('open page, clk ex_1, clk the first link', async () => {
        await ShiftingContent.open();
        await ShiftingContent.ex_1.click();
        const loc_1 = await ShiftingContent.gallery.getLocation();

        await ShiftingContent.clk_random.click();
        await expect(browser).not.toHaveUrl('https://the-internet.herokuapp.com/shifting_content/menu');

        const loc_2 = await ShiftingContent.gallery.getLocation();
        
        if (loc_1 !== loc_2){
            await ShiftingContent.clk_random.click();
        } 
        
        
    });

    it('clk ex_1, clk the second link', async () => {
        await ShiftingContent.open();
        await ShiftingContent.ex_1.click();
        const loc_1 = await ShiftingContent.gallery.getLocation();

        await ShiftingContent.clk_shift_100.click();
        await expect(browser).not.toHaveUrl('https://the-internet.herokuapp.com/shifting_content/menu');

        const loc_2 = await ShiftingContent.gallery.getLocation();
        await expect(loc_1).not.toEqual(loc_2);
    });

    it('clk ex_1, clk the third link', async () => {
        await ShiftingContent.open();
        await ShiftingContent.ex_1.click();

        await ShiftingContent.clk_together.click();
        await expect(browser).not.toHaveUrl('https://the-internet.herokuapp.com/shifting_content/menu');

        
    });

    it('clk ex_2', async () => {
        await ShiftingContent.open();
        await ShiftingContent.ex_2.click();
        await expect(browser).toHaveUrl('https://the-internet.herokuapp.com/shifting_content/image');

        const atr_1 = await ShiftingContent.image.getAttribute('src');
        await ShiftingContent.image_changed.click();
        const atr_2 = await ShiftingContent.image.getAttribute('src');
        await expect(atr_1).not.toBe(atr_2)
        
    });

    it('negative test,ex_2, browser refresh, url not changed and img too', async () => {
        await browser.refresh()
        await expect(browser).not.toHaveUrl('https://the-internet.herokuapp.com/shifting_content/image');

        const atr_1 = await ShiftingContent.image.getAttribute('src');
        await expect(atr_1).not.toBe("/img/avatar.jpg")
        
    });
    
    it('clk ex_2, clk the second link, check changes', async () => {
        await ShiftingContent.open();
        await ShiftingContent.ex_2.click();
        const loc_1 = await ShiftingContent.image.getLocation();

        await ShiftingContent.clk_shift_100.click();
        await expect(browser).not.toHaveUrl('https://the-internet.herokuapp.com/shifting_content/image');

        const loc_2 = await ShiftingContent.image.getLocation();
        await expect(loc_1).not.toEqual(loc_2);
    });


    it('clk ex_3, get text of element and browser refresh', async () => {
        await ShiftingContent.open();
        await ShiftingContent.ex_3.click();
        await expect(browser).toHaveUrl('https://the-internet.herokuapp.com/shifting_content/list');

        const loc_1 = await  ShiftingContent.ex_3_text.getText();
        await browser.refresh();
        const loc_2 = await  ShiftingContent.ex_3_text.getText();
        await expect(loc_1).not.toBe(loc_2)
    });

});
