const path = require('path')
const fs = require('fs')
const { URL } = require('url')
const assert = require('assert');
const waitforfileexist = require('../../util/waitForFileExist');


describe('Downloads', function () {
    it('should download the file', function () {
      browser.url('./download')
      const downloadlink = $('*=Zoom.pkg');
      downloadlink.click();
      const downloadHref = downloadLink.getAttribute('href');
      const downloadUrl = new URL(downloadHref);
      const fullPath = downloadUrl.pathname; // -> 'download/Zoom.pkg'
      const splitPath = fullPath.split('/'); // -> ['download','Zoom.pkg']
      const fileName = splitPath.splice(-1)[0]; // -> 'Zoom.pkg'
      const filePath = path.join(global.downloadDir, fileName); // ->/path/to/wdio/test/tempDownload/Zoom.pkg
      
      browser.call(function (){
        return waitforfileexist(filePath, 60000)
      });

      const fileContents = fs.readFileSync(filePath, 'utf-8')
      assert.ok(fileContents.includes('asdf'))
    
    })
})